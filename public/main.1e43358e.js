// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  return newRequire;
})({"src/CST.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CST = void 0;
var CST = {
  SCENES: {
    LOAD: "LOAD",
    MENU: "MENU",
    PLAY: "PLAY"
  }
};
exports.CST = CST;
},{}],"src/scenes/playScene.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.playScene = void 0;

var _CST = require("../CST");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var playScene =
/*#__PURE__*/
function (_Phaser$Scene) {
  _inherits(playScene, _Phaser$Scene);

  function playScene() {
    var _this;

    _classCallCheck(this, playScene);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(playScene).call(this, {
      key: _CST.CST.SCENES.PLAY
    }));
    _this.player;
    _this.aliens;
    _this.bullets;
    _this.InvaderBullet;
    _this.bulletTime = 0;
    _this.cursors;
    _this.fireButton;
    _this.starfield;
    _this.score = 0;
    _this.scoreString = "";
    _this.scoreText;
    _this.lives;
    _this.stateText;
    _this.playerBullets;
    _this.alienBullets;
    _this.container;
    _this.playerBulletTimeEvent;
    _this.alienTween;
    _this.debug_scene;
    return _this;
  }

  _createClass(playScene, [{
    key: "init",
    value: function init() {
      console.log("started playscene");
    }
  }, {
    key: "preload",
    value: function preload() {
      this.load.image('bullet', './bullet.png');
      this.load.image('enemyBullet', './enemy-bullet.png');
      this.load.spritesheet('invader', './invader32x32x4.png', {
        frameWidth: 32,
        frameHeight: 32
      });
      this.load.image('ship', './player.png');
      this.load.spritesheet('kaboom', './explode.png', {
        frameWidth: 128,
        frameHeight: 128
      });
      this.load.image('starfield', './starfield.png');
    }
  }, {
    key: "create",
    value: function create() {
      //configurar bordes del mundo para que tengan colision
      this.physics.world.setBoundsCollision(true, true, true, true); //Fondo de estrellas

      this.starfield = this.add.tileSprite(0, 0, window.innerWidth, window.innerHeight, 'starfield').setOrigin(0, 0);
      this.player = this.physics.add.image(window.innerWidth / 2, window.innerHeight - 50, 'ship'); //Crea la nave usando el sistema de fisicas de Phaser 3

      this.player.setScale(2);
      this.player.body.setAllowGravity(false); //Desactiva la gravedad en el objeto nave

      this.player.setCollideWorldBounds(true); //Abilita colision con los border de la escena

      this.player.setInteractive();
      this.input.setDraggable(this.player);
      this.playerBullets = this.add.group(); //Creacion de grupo para balas jugador

      this.alienBullets = this.add.group(); //Creacion de grupo para balas alien
      //animacion de invasores

      this.anims.create({
        key: 'fly',
        frames: this.anims.generateFrameNumbers('invader', {
          start: 0,
          end: 3
        }),
        frameRate: 20,
        repeat: -1
      }); //animacion de explosion

      this.anims.create({
        key: 'boom',
        frames: this.anims.generateFrameNumbers('kaboom', {
          start: 0,
          end: 15,
          first: 0
        }),
        hideOnComplete: true //repeat: 1

      });
      /*
      funcion para llamar aliens. se usa la funcion call() para llamar a la funcion createAliens
      Desde el scope de la funcion create de esta escena. De lo contrario tendria que acceder a la
      escena directamente usando game.scene.scenes[0] para poder usar funciones como fisicas.
                   https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Function/call
                   */

      this.createAliens(); //.call(this);
      //  Puntaje

      this.scoreString = 'Score : ';
      this.scoreText = this.add.text(10, 10, this.scoreString + this.score, {
        font: '34px Arial',
        fill: '#fff'
      }); //  configuracion de Vidas

      this.lives = this.add.group();
      this.add.text(10, 50, 'Vidas : ', {
        font: '34px Arial',
        fill: '#fff'
      }); //configuracion de vidas, se usa el mismo sprite para     

      for (var i = 0; i < 2; i++) {
        var ship = this.lives.create(25 + 30 * i, 100, 'ship');
        ship.angle = 90;
        ship.alpha = 0.9;
      } // Texto De estado de juego
      //stateText = this.add.text(50, 150, '', { font: '60px Arial', fill: '#fff' });
      //stateText.visible = false;


      this.stateText = this.make.text({
        x: window.innerWidth / 2,
        y: window.innerHeight / 2,
        text: '',
        origin: {
          x: 0.5,
          y: 0.5
        },
        style: {
          font: 'bold 25px Arial',
          fill: 'white',
          wordWrap: {
            width: 300
          }
        }
      });
      this.stateText.visible = false; //Controles de fechas de teclado

      this.input.on('drag', function (pointer, gameObject, dragX, dragY) {
        gameObject.x = dragX; //gameObject.body.velocity.x = dragX; 
      });
      /*
      this.input.on('pointerdown', function(pointer){
          //this.scene.scene.time.now
          console.log(this.scene.time.now);
          if (this.scene.time.now > bulletTime) {
                        bullet = game.scene.scenes[0].physics.add.image(player.x, player.y - 8, 'bullet').setVelocity(0, -400).setCollideWorldBounds(true);
              bullet.body.setAllowGravity(false);
              bullet.body.onWorldBounds = true;
              playerBullets.add(bullet);
              bulletTime = game.scene.scenes[0].sys.time.now + 200;
       
                   }
       });*/
      //funcion para destruir los objetos que tocan con los bordes del mundo

      this.physics.world.on('worldbounds', function (body) {
        return body.gameObject.destroy();
      }); //funcion para que los aliens disparen comentada

      this.eventoDeTiempo = this.time.addEvent({
        loop: true,
        callback: this.aliensDisparan,
        callbackScope: this,
        delay: 3000
      }); //funcion para disparar cada cierto tiempo

      this.playerBulletTimeEvent = this.time.addEvent({
        loop: true,
        callback: this.fireBullet,
        callbackScope: this,
        delay: 1000
      }); //collider balas jugador con alien

      this.physics.add.collider(this.playerBullets, this.aliens, function (bullet, alien) {
        bullet.destroy();
        alien.destroy(); //  Increase the score

        this.score += 20;
        this.scoreText.text = this.scoreString + this.score;
        var bomb = this.add.sprite(alien.x + this.container.x, alien.y + this.container.y, "kaboom").play("boom");

        if (this.aliens.getChildren().length == 0) {
          this.score += 1000;
          this.scoreText.text = this.scoreString + this.score;
          this.textoFinalJuego.call(this, true);
        }
      }, undefined, this); //collider balas alien con jugador

      this.physics.add.collider(this.alienBullets, this.player, function (alienBullet, player) {
        alienBullet.destroy();
        this.add.sprite(this.player.x, this.player.y, "kaboom").play("boom");

        if (this.lives.getChildren().length == 1) {
          //funcion para remover evento de tiempo en loop
          this.eventoDeTiempo.remove(false);
          this.alienTween.stop();
          this.playerBulletTimeEvent.paused = true;
          this.player.destroy();
          this.textoFinalJuego.call(this, false);
        } else {
          this.lives.getFirstAlive().destroy();
          this.alienTween.stop();
          this.container.x = 100;
          this.container.y = 50;
          this.alienTween.resume();
          this.player.x = window.innerWidth / 2;
          this.player.y = window.innerHeight - 50;
        }
      }, undefined, this); //collider entre aliens y jugador

      this.physics.add.collider(this.player, this.aliens, function (player, alien) {
        alien.destroy();

        if (this.lives.getChildren().length == 0) {
          //funcion para remove evento de tiempo en loop
          this.eventoDeTiempo.remove(false);
          this.alienTween.stop();
          this.textoFinalJuego.call(this, false);
        } else {
          this.lives.getFirstAlive().destroy();
          this.alienTween.stop();
          this.container.x = 100;
          this.container.y = 50;
          this.alienTween.resume();
          this.player.x = window.innerWidth / 2;
          this.player.y = window.innerHeight - 50;
        }
      }, undefined, this);
    } //fin de phaser create

  }, {
    key: "update",
    value: function update() {
      //  Scroll the background -- Mover el Fondo 
      this.starfield.tilePositionY += 2;

      if (this.player.active) {
        //  Reset the player, then check for movement keys
        this.player.body.velocity.setTo(0, 0);
        /*
                        if (cursors.left.isDown) { player.body.velocity.x = -200; }
                        else if (cursors.right.isDown) { player.body.velocity.x = 200; }*/
        //if (restartButton.isDown) { this.scene.restart(); }
      }
    } //fin de funcion update de Phaser 3
    //Funcion Para crear un evento de escucha cuando se hace un click
    //reinica la escene Phaser

  }, {
    key: "reiniciaElJuego",
    value: function reiniciaElJuego() {
      this.input.addListener('pointerdown', function (pointer) {
        this.scene.restart();
      }, this);
    }
  }, {
    key: "fireBullet",
    value: function fireBullet() {
      //Funcion Que dispara una bala al presionar espacio
      //  To avoid them being allowed to fire too fast we set a time limit
      //game.scene.scenes[0].sys.time.now this.scene.scene.time.now
      if (this.sys.time.now > this.bulletTime) {
        var bullet = this.physics.add.image(this.player.x, this.player.y - 8, 'bullet').setVelocity(0, -400).setCollideWorldBounds(true);
        bullet.body.setAllowGravity(false);
        bullet.body.onWorldBounds = true;
        this.playerBullets.add(bullet);
        this.bulletTime = this.sys.time.now + 200; //game.scene.scenes[0].sys.time.now + 200;
      }
    }
  }, {
    key: "aliensDisparan",
    value: function aliensDisparan() {
      //Tomamos un alien vivo al azar
      var randomAlien = this.aliens.getChildren()[Phaser.Math.Between(0, this.aliens.getChildren().length - 1)];

      if (randomAlien) {
        var InvaderBullet = this.physics.add.image(randomAlien.x + this.container.x, randomAlien.y + this.container.y, "enemyBullet").setCollideWorldBounds(true);
        InvaderBullet.body.setAllowGravity(false);
        InvaderBullet.body.onWorldBounds = true;
        this.physics.moveToObject(InvaderBullet, this.player, 150);
        this.alienBullets.add(InvaderBullet);
      }
    }
  }, {
    key: "createAliens",
    value: function createAliens() {
      var _this2 = this;

      //Los Aliens
      //Para registrar colision los aliens tienen que estar en un grupo de fisica Phaser.
      this.aliens = this.physics.add.group({});

      for (var y = 0; y < 4; y++) {
        for (var x = 0; x < 5; x++) {
          var alien = this.aliens.create(x * 48, y * 50, 'invader');
          alien.anims.play('fly');
          alien.body.moves = false;
        }
      } //Los grupos en Phaser 3 no tienen metodos de ajuste de posicion asi que
      //tenemos que usar los contenedores de Phaser para ajustar la posicion del grupo de aliens.


      this.container = this.add.container(0, 50);
      this.container.add(this.aliens.getChildren()); //Los tween de Phaser 3 permite el movimientos de un objeto o grupo.
      //movimiento alien

      console.log(this);
      this.alienTween = this.tweens.add({
        targets: this.container,
        //objeto que contiene el grupo de aliens
        x: 300,
        duration: 3000,
        loop: -1,
        yoyo: true,
        ease: 'Linear.easeInOut',
        onLoop: function onLoop() {
          _this2.container.y += 10;
        },
        onYoyo: function onYoyo() {
          _this2.container.y += 10;
        }
      });
    }
  }, {
    key: "textoFinalJuego",
    value: function textoFinalJuego(estado) {
      if (estado) {
        this.stateText.visible = true;
        this.stateText.text = "Has Ganado,has click para reiniciar";
        this.reiniciaElJuego.call(this);
      }

      if (!estado) {
        this.stateText.visible = true;
        this.stateText.text = "Has Perdido,has click para reiniciar"; //player.body.enable = false;

        this.reiniciaElJuego.call(this);
      }
    }
  }]);

  return playScene;
}(Phaser.Scene);

exports.playScene = playScene;
},{"../CST":"src/CST.js"}],"src/main.js":[function(require,module,exports) {
"use strict";

var _playScene = require("./scenes/playScene");

/** @type {import ("../typings/phaser") }*/
window.onload = function () {
  var config = {
    type: Phaser.AUTO,
    parent: "#phaserGameCanvas",
    height: 800,
    width: 600,
    physics: {
      default: 'arcade',
      arcade: {
        gravity: {
          y: 200
        },
        debug: false
      }
    },
    scene: _playScene.playScene
  };
  var game = new Phaser.Game(config);
};
},{"./scenes/playScene":"src/scenes/playScene.js"}],"../../../../../../../usr/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "34875" + '/');

  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      console.clear();
      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id);
  });
}
},{}]},{},["../../../../../../../usr/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","src/main.js"], null)
//# sourceMappingURL=/main.1e43358e.map